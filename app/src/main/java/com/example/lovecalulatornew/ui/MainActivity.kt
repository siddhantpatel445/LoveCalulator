package com.example.lovecalulatornew.ui

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.lovecalulatornew.R
import com.example.lovecalulatornew.databinding.ActivityMainBinding
import com.example.lovecalulatornew.viewmodel.MainViewModel
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //To make marquee moving
        binding.textView.isSelected = true

        if (!isConnected) {
            showCustomDialog()
        }

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        binding.viewmodel = viewModel

        viewModel.progressb.observe(this) {
            if (it) {
                binding.calculate.hideKeyboard()
                binding.progressBar.visibility = View.VISIBLE
                binding.circleProgress.visibility = View.INVISIBLE
                binding.percentage.visibility = View.INVISIBLE
                binding.loveMessage.visibility = View.INVISIBLE
            } else {
                binding.progressBar.visibility = View.INVISIBLE
                binding.circleProgress.visibility = View.VISIBLE
                binding.percentage.visibility = View.VISIBLE
                binding.loveMessage.visibility = View.VISIBLE
            }
        }

        viewModel.results.observe(this) {
            binding.loveMessage.text = it.result
            binding.percentage.text = "${it.percentage}%"
            binding.circleProgress.apply {
                progressMax = 100f
                setProgressWithAnimation(it.percentage.toFloat(), 1000)
                progressBarWidth = 15f
            }
        }

        viewModel.connectionMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            binding.animationView.visibility = View.VISIBLE
            binding.circleProgress.visibility = View.INVISIBLE
            binding.percentage.visibility = View.INVISIBLE
            binding.loveMessage.visibility = View.INVISIBLE
            binding.progressBar.visibility = View.INVISIBLE
            binding.fname.visibility = View.INVISIBLE
            binding.lname.visibility = View.INVISIBLE
            binding.textView.visibility = View.INVISIBLE
            binding.calculate.visibility = View.INVISIBLE
        }

        viewModel.emptyStrings.observe(this) {
            if (it) {
                Toast.makeText(this, "Please fill in both names", Toast.LENGTH_LONG).show()
                binding.animationView.visibility = View.INVISIBLE
                binding.circleProgress.visibility = View.INVISIBLE
                binding.percentage.visibility = View.INVISIBLE
                binding.loveMessage.visibility = View.INVISIBLE
                binding.progressBar.visibility = View.INVISIBLE
            }
        }
    }

    private fun View.hideKeyboard() {
        val inputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun showCustomDialog() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage("It seems you are not connect to the Internet, Please turn on your WIFI or Mobile Data and Try Again")
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setPositiveButton("Ok") { _, _ -> finish() }
        alertDialogBuilder.show()
    }

    private val Context.isConnected: Boolean
        get() {
            return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo?.isConnected == true
        }
}
