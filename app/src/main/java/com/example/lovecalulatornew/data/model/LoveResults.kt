
data class LoveResults(
    val fname: String,
    val sname: String,
    val percentage: Int = 0,
    val result: String
)

