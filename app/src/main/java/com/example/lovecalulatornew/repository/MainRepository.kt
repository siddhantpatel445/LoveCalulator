package com.example.lovecalulatornew.repository

import LoveResults
import androidx.lifecycle.MutableLiveData
import timber.log.Timber

class MainRepository {

    var results = MutableLiveData<LoveResults>()

    suspend fun getResults(fname: String, sname: String) {
        results.value = RetrofitBuilder.apiService.getLoversResult(fname, sname)
        Timber.d(results.value.toString())
    }
}